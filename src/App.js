import React from 'react';
import './App.css';
import ValutePicker from './Components/ValutePicker';
import TitlePage from './Components/TitlePage';
import ModalWelcome from './Components/Modal';




function App() {
  let firstVisit = sessionStorage.getItem("firstVisit");

  return (
    <div className="App">
      {firstVisit == null ? <ModalWelcome/> : null}
      <TitlePage/>
      <ValutePicker/>
    </div>
  );
}

export default App;
