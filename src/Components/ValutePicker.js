import React from "react";
import axios from "axios";
import { Row } from "react-bootstrap";
import Fade from 'react-reveal/Fade';


class ValuteConverter extends React.Component {

  state = {
    result: null,
    fromCurrency: "EUR",
    toCurrency: "HRK",
    amount: 1,
    currencies: []
  };

  componentDidMount() {
    axios
      .get("https://api.ratesapi.io/api/2010-01-12?base")
      .then(response => {
        const currencyAr = ["EUR"];
        for (const key in response.data.rates) {
          currencyAr.push(key);
        }
        this.setState({ currencies: currencyAr });
      })
      .catch(error => {
        console.log("Error occured. Please try again later.", error);
      });
  }
  convertHandler = () => {
    if (this.state.fromCurrency !== this.state.toCurrency) {
      axios
        .get(
          `https://api.ratesapi.io/api/2010-01-12?base=${
          this.state.fromCurrency
          }&symbols=${this.state.toCurrency}`
        )
        .then(response => {
          const result =
            this.state.amount * response.data.rates[this.state.toCurrency];
          this.setState({ result: result.toFixed(2) });
        })
        .catch(error => {
          console.log("Error occured. Please try again later.", error.message);
        });
    } else {
      this.setState({ result: "Hey, its the same!" });
    }
  };
  selectHandler = event => {
    if (event.target.name === "from") {
      this.setState({ fromCurrency: event.target.value });
    } else {
      if (event.target.name === "to") {
        this.setState({ toCurrency: event.target.value });
      }
    }
  };
  render() {
    return (
      <Fade clear>
      <div>
        <Row>
          <div className="col-md-3"/>
          <div className="col-md-1"/>
          <div className="col-md-2 mt-5">
            <input
              className="form-control"
              name="amount"
              type="text"
              value={this.state.amount}
              onChange={event => this.setState({ amount: event.target.value })}
            />
          </div>
          <div className="col-md-3 mt-5">
            <select
              className="custom-select col-md-3"
              name="from"
              onChange={event => this.selectHandler(event)}
              value={this.state.fromCurrency}
            >
              {this.state.currencies.map(currency => (
                <option key={currency}>{currency}</option>
              ))}
            </select>
            <select
              name="to"
              className="custom-select col-md-3 ml-1"
              onChange={event => this.selectHandler(event)}
              value={this.state.toCurrency}
            >
              {this.state.currencies.map(currency => (
                <option key={currency}>{currency}</option>
              ))}
            </select>
            <button className="btn btn-dark ml-3" onClick={this.convertHandler}>Convert</button>
          </div>

        </Row>
        <Row>
          <div className="col-md-5 mt-5"/>
        {this.state.result && <h2 className="mt-5">  
          Result: {this.state.result}</h2>}
        </Row>
      </div> </Fade> 
    );
  }
}
export default ValuteConverter;