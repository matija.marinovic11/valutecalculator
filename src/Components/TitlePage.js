import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';



class TitlePage extends Component {

  componentDidMount(){
    sessionStorage.setItem("firstVisit",false);
  }

  render() {

    return <div className="Title">
      <Fade top>
      Valute Converter
      
      <h1> Explore rates for 30+ currencies</h1>
      </Fade>
    </div>

  }

}

export default TitlePage;