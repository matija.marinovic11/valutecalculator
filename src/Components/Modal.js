import React,{useState} from 'react';
import {Modal,Button} from 'react-bootstrap';

function ModalWelcome() {
    const [show, setShow] = useState(true);
  
    const handleClose = () => setShow(false);
  
    return (
      <>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Valute Converter App</Modal.Title>
          </Modal.Header>
          <Modal.Body>You’re so cool bro, thank you for using this magnificent currency app!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }

  export default ModalWelcome;
  